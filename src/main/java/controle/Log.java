package controle;

import java.util.logging.*;

public class Log {

    public static Logger logger;
    private static FileHandler fh;
    private static final String logPath = "/home/mmed/dev/log/<nameFile>.log";

    public Log(String name, String nameFile){

        try {

            logger = Logger.getLogger(name);
            fh = new FileHandler(logPath.replace("<nameFile>", nameFile));
            logger.addHandler(fh);
            logger.setUseParentHandlers(true);

            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(new MyFormatterLog());

        }catch (Exception e){

        }
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}
