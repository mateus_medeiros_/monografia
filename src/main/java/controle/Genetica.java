package controle;

import entity.Individuo;

import java.util.*;
import java.util.logging.Logger;

public class Genetica {


    private List<Integer> l1 = Arrays.asList(1, 2, 3);
    private List<Integer> l2 = Arrays.asList(1, 3, 2);
    private List<Integer> l3 = Arrays.asList(2, 1, 3);
    private List<Integer> l4 = Arrays.asList(2, 3, 1);
    private List<Integer> l5 = Arrays.asList(3, 1, 2);
    private List<Integer> l6 = Arrays.asList(3, 2, 1);

    private List<List<Integer>> listaDeListas;


    //tentar ver uma forma melhor de tornar isso aqui estático
    public Genetica() {

        listaDeListas = new ArrayList<List<Integer>>();

        listaDeListas.add(l1);
        listaDeListas.add(l2);
        listaDeListas.add(l3);
        listaDeListas.add(l4);
        listaDeListas.add(l5);
        listaDeListas.add(l6);

    }

    private List<Integer> pontosDeCorte(){

        Random gerador = new Random();

        int tamanhoLista = listaDeListas.size();

        return listaDeListas.get(gerador.nextInt(tamanhoLista));
    }

    /**
     *   seria interessante já atribuir a mutação ? ainda não.
     *   nem todos devem sofrer mutação, podemos tentar depois
     *   mais pra frente.
     * **/
    private List<Individuo> individuosValidos(Individuo filhoA, Individuo filhoB){

        List<Individuo> filhos = new ArrayList<Individuo>();

        if(filhoA.valido()){
            // a.mutação
            filhos.add(filhoA);
        }

        if(filhoB.valido()){
            //b.mutação
            filhos.add(filhoB);
        }
        return filhos;
    }

    public void mutacaoAleatoria(List<Individuo> individuos, Integer qtdMutacao, Logger log){

        Random gerador = new Random();

        for (int i = 0 ; i < qtdMutacao; i++){

            int pos = gerador.nextInt(individuos.size() + 1);

            // faça a mutação apenas se existir a posição
            if( pos < individuos.size()){
                individuos.get(pos).mutacao(log);
            }
        }
    }

    public List<Individuo> cruzamento(Individuo a, Individuo b){


        List<Integer> pontosDeCorte = pontosDeCorte();

        for (Integer corte : pontosDeCorte){

            if (corte == 0) {

                Individuo filhoZero1 = new Individuo(a.getForca(), b.getInteligencia(), b.getAgilidade(), b.getDestreza());
                Individuo filhoZero2 = new Individuo(b.getForca(), a.getInteligencia(), a.getAgilidade(), a.getDestreza());

                List<Individuo> filhosValidos = individuosValidos(filhoZero1, filhoZero2);

                if(!filhosValidos.isEmpty()){
                    return filhosValidos;
                }
            }

            if (corte == 1) {

                Individuo filhoUm1 = new Individuo(a.getForca(), a.getInteligencia(), b.getAgilidade(), b.getDestreza());
                Individuo filhoUm2 = new Individuo(b.getForca(), b.getInteligencia(), a.getAgilidade(), a.getDestreza());

                List<Individuo> filhosValidos = individuosValidos(filhoUm1, filhoUm2);

                if(!filhosValidos.isEmpty()){
                    return filhosValidos;
                }
            }

            if (corte == 2) {

                Individuo filhoDois1 = new Individuo(a.getForca(), a.getInteligencia(), a.getAgilidade(), b.getDestreza());
                Individuo filhoDois2 = new Individuo(b.getForca(), b.getInteligencia(), b.getAgilidade(), a.getDestreza());

                List<Individuo> filhosValidos = individuosValidos(filhoDois1, filhoDois2);

                if(!filhosValidos.isEmpty()){
                    return filhosValidos;
                }
            }


        }

        System.out.println("em nenhum dos 3 cortes o individuo é valido");
        return null;
    }


}
