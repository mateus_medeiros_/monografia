package controle;


import entity.Ambiente;
import enuns.Classe;
import enuns.Clima;
import enuns.Terreno;

import java.util.*;

public class MatrizBase {
    /*
     cima cima baixo baixo
     cima baixo cima baixo
     cima baixo baixo cima
     baixo baixo cima cima
   */
    public static Map<String, Ambiente> matriz;

    private List<Integer> gp1 = Arrays.asList(1, 1, 0, 0);
    private List<Integer> gp2 = Arrays.asList(0, 0, 1, 1);

    private List<Integer> gp3 = Arrays.asList(1, 0, 1, 0);
    private List<Integer> gp4 = Arrays.asList(0, 1, 0, 1);

    private List<Integer> gp5 = Arrays.asList(1, 0, 0, 1);
    private List<Integer> gp6 = Arrays.asList(0, 1, 1, 0);


    private List<List<Integer>> ganhaOuPerdeListas;

    public MatrizBase() {

        matriz = new HashMap<String, Ambiente>();

        ganhaOuPerdeListas = new ArrayList<List<Integer>>();

        ganhaOuPerdeListas.add(gp1);
        ganhaOuPerdeListas.add(gp2);
        ganhaOuPerdeListas.add(gp3);
        ganhaOuPerdeListas.add(gp4);
        ganhaOuPerdeListas.add(gp5);
        ganhaOuPerdeListas.add(gp6);

        geraMatriz(Classe.GUERREIRO);
        geraMatriz(Classe.MAGO);
        geraMatriz(Classe.ASSASSINO);
        geraMatriz(Classe.ARQUEIRO);

        Log log = new Log("matriz", "matriz");

        log.getLogger().info(matriz.toString());

    }

    public void geraMatriz(Classe classe){

        for (Terreno terreno: Terreno.values()){

            //embaralha a lista
            Collections.shuffle(ganhaOuPerdeListas);

            int i = 0;

            for (Clima clima : Clima.values()){

                Ambiente ambiente = new Ambiente(classe, terreno, clima, ganhaOuPerdeListas.get(i));

                matriz.put(classe+"+"+terreno+"+"+clima, ambiente);

                i++;
            }
        }

    }


}
