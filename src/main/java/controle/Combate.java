package controle;

import entity.Ambiente;
import entity.Individuo;
import enuns.Clima;
import enuns.Terreno;

import java.util.*;

public class Combate {

    private Random gerador;

    private Map<String, Ambiente> matriz;

    private Integer pontosA;
    private Integer pontosB;

    public Combate() {

        pontosA = 0;
        pontosB = 0;
        gerador = new Random();
        this.matriz = MatrizBase.matriz;
    }

    private Terreno terrenoAleatorio(){

        List<Terreno> listEnums = Collections.unmodifiableList(Arrays.asList(Terreno.values()));

        return listEnums.get(gerador.nextInt(listEnums.size()));
    }

    private Clima climaAleatorio(){

        List<Clima> listEnums = Collections.unmodifiableList(Arrays.asList(Clima.values()));

        return listEnums.get(gerador.nextInt(listEnums.size()));
    }


    private Ambiente encontreAmbiente(Individuo a, Terreno terreno, Clima clima){

        return matriz.get(a.getClasse()+"+"+terreno+"+"+clima);
    }

    private void ambientacao(Individuo a, Individuo b, StringBuilder stringBuilder){

        Clima clima = climaAleatorio();
        Terreno terreno = terrenoAleatorio();


        stringBuilder.append("terreno: " + terreno +"\n");
        stringBuilder.append("clima: " + clima+"\n");

        Ambiente ambienteIndividuoA = encontreAmbiente(a, terreno, clima);
        Ambiente ambienteIndividuoB = encontreAmbiente(b, terreno, clima);

            Individuo cloneA = a.clone();
            Individuo cloneB = b.clone();

        stringBuilder.append("individuo - A: " + cloneA +"\n");
        stringBuilder.append("individuo - B: " + cloneB +"\n");

            cloneA.sofraComAmbiente(ambienteIndividuoA);
            cloneB.sofraComAmbiente(ambienteIndividuoB);

        stringBuilder.append("sofreu com o ambiente individuo A: " + cloneA +"\n");
        stringBuilder.append("sofreu com o ambiente individuo B: " + cloneB +"\n");

            calcPontosDeForca(cloneA, cloneB);
            calcPontosDeInteligencia(cloneA, cloneB);
            calcPontosDeAgilidade(cloneA, cloneB);
            calcPontosDeDestreza(cloneA, cloneB);

    }

    private void calcPontosDeForca(Individuo a, Individuo b){

        if(a.getForca() > b.getForca()){
            //ponto para o A
            pontosA = pontosA + 1;

        }else if(a.getForca() < b.getForca()){
            // ponto para o B
            pontosB = pontosB + 1;
        }

    }

    private void calcPontosDeInteligencia(Individuo a, Individuo b){

        if(a.getInteligencia() > b.getInteligencia()){
            //ponto para o A
            pontosA = pontosA + 1;

        }else if(a.getInteligencia() < b.getInteligencia()){
            // ponto para o B
            pontosB = pontosB + 1;
        }

    }

    private void calcPontosDeAgilidade(Individuo a, Individuo b){

        if(a.getAgilidade() > b.getAgilidade()){
            //ponto para o A
            pontosA = pontosA + 1;

        }else if(a.getAgilidade() < b.getAgilidade()){
            // ponto para o B
            pontosB = pontosB + 1;
        }

    }

    private void calcPontosDeDestreza(Individuo a, Individuo b){

        if(a.getDestreza() > b.getDestreza()){
            //ponto para o A

            pontosA = pontosA + 1;


        }else if(a.getDestreza() < b.getDestreza()){
            // ponto para o B
            pontosB = pontosB + 1;
        }

    }


    public Individuo batalha(Individuo a, Individuo b, Log log){


        StringBuilder stringBuilder = new StringBuilder();

        if(a.getId().equals(b.getId())){
            return null;
        }

        stringBuilder.append("===========================================\n");

        ambientacao(a, b, stringBuilder);

        if(pontosA > pontosB){

            stringBuilder.append("---------------------\n");
            stringBuilder.append("o vencedor foi: " + a + "\n");
            stringBuilder.append("---------------------" + "\n");

            a.incrementVitoria();

            footerLog(stringBuilder, log);

            return a;

        }else if(pontosB > pontosA){


            stringBuilder.append("---------------------\n");
            stringBuilder.append("o vencedor foi: " + b + "\n");
            stringBuilder.append("---------------------" + "\n");

            b.incrementVitoria();

            footerLog(stringBuilder, log);

            return b;
        }

        stringBuilder.append("EMPATE");
        footerLog(stringBuilder, log);


        return null;
    }


    public void footerLog(StringBuilder stringBuilder, Log log){
        stringBuilder.append("===========================================\n");

        if(log != null){
            log.getLogger().info(stringBuilder.toString());
        }
    }

}
