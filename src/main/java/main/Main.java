package main;

import controle.Combate;
import controle.Genetica;
import controle.Log;
import controle.MatrizBase;
import entity.Ambiente;
import entity.Geracao;
import entity.Individuo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public void teste1(){
        Individuo individuoA = new Individuo();

        Individuo individuoB = new Individuo();

        //System.out.println(individuo);

        Genetica genetica = new Genetica();

        List<Individuo> filhos = genetica.cruzamento(individuoA, individuoB);

        System.out.println("A: " + individuoA);
        System.out.println("B: " + individuoB);
        System.out.println("filhos: " + filhos);

        //filhos.get(0).mutacao();
        System.out.println("filho com mutação: " + filhos.get(0));

        MatrizBase matrizBase = new MatrizBase();

        System.out.println("================");
        //matrizBase.geraMatriz(Classe.GUERREIRO);

        Ambiente ambiente = new Ambiente();
        //ambiente.sorteiaAcrescimoDecrescimo();


        Combate combate = new Combate();

        Individuo individuoVencedor = combate.batalha(individuoA, individuoB, null);

        System.out.println("individuo vencedor = " + individuoVencedor);

        //Combate combate = new Combate(matrizBase.getMatriz());
        //combate.batalha(individuoA, individuoB);

    }

    public void test2(){

        //Logger logger = new Log().init("Start Log");

        //logger.info("========= start =========");

        MatrizBase matrizBase = new MatrizBase();

        Geracao geracao = new Geracao(100, null);
        geracao.Lutem();

        Collections.sort(geracao.getIndividuoList(), Individuo.comparatorNumVitorias);
        Collections.reverse(geracao.getIndividuoList());

        for (Individuo individuo: geracao.getIndividuoList()){
            System.out.println(individuo);
        }

    }

    public void teste3(){


        MatrizBase matrizBase = new MatrizBase();
        //Log log = new Log();

        Geracao geracaoZero = new Geracao(100, null);
        geracaoZero.Lutem();

        List<Individuo> filhosGeracaoZero = geracaoZero.filhosDessaGeracao();

        Geracao geracaoUm = new Geracao(100, filhosGeracaoZero);
        geracaoUm.Lutem();

    }

    public void testeMelhorFilho(){

        List<Individuo> teste = new ArrayList<Individuo>();

        Individuo a = new Individuo(1, 2, 3, 4);
        Individuo b = new Individuo(1, 2, 3, 4);
        Individuo c = new Individuo(1, 1, 1, 1);
        Individuo d = new Individuo(2, 2, 2, 2);
        Individuo e = new Individuo(2, 2, 2, 2);
        Individuo f = new Individuo(2, 2, 2, 2);

        teste.add(a);
        teste.add(b);
        teste.add(c);
        teste.add(d);
        teste.add(e);
        teste.add(f);

        Geracao ge = new Geracao(0, new ArrayList<Individuo>());

        ge.setFilhosList(teste);

        System.out.println(ge.melhorFilho());


    }

    public void firstTestConvertion(){

        MatrizBase matrizBase = new MatrizBase();

        List<Geracao> geracaoList = new ArrayList<Geracao>();

        List<Individuo> filhosGeracao = null;

        int numGeracoes = 100;

        //faça apenas 60 gerações
        for(int i = 1 ; i <= numGeracoes; i++){

            Geracao geracao = new Geracao(200, filhosGeracao);
            geracao.Lutem();

            geracaoList.add(geracao);

            filhosGeracao = new ArrayList<Individuo>();
            filhosGeracao.addAll(geracao.filhosDessaGeracao());
        }

        Individuo filhoGeracao = geracaoList.get(numGeracoes-1).melhorFilho();

        humanosVsMaquina(filhoGeracao, individuosHumanos());

    }


    public List<Individuo> individuosHumanos(){

        List<Individuo> personagensDeHumanos = new ArrayList<Individuo>();

        personagensDeHumanos.add(new Individuo(30, 80, 70, 20));
        personagensDeHumanos.add(new Individuo(40, 80, 40, 40));
        personagensDeHumanos.add(new Individuo(40, 30, 60, 70));
        personagensDeHumanos.add(new Individuo(20, 40, 80, 60));
        personagensDeHumanos.add(new Individuo(40, 60, 60, 40));
        personagensDeHumanos.add(new Individuo(0, 40, 60, 100));
        personagensDeHumanos.add(new Individuo(40, 80, 40, 40));
        personagensDeHumanos.add(new Individuo(60, 80, 30, 30));
        personagensDeHumanos.add(new Individuo(20, 60, 60, 60));
        personagensDeHumanos.add(new Individuo(40, 80, 40, 40));
        personagensDeHumanos.add(new Individuo(75, 75, 25, 25));
        personagensDeHumanos.add(new Individuo(40, 60, 60, 40));
        personagensDeHumanos.add(new Individuo(40, 60, 40, 60));
        personagensDeHumanos.add(new Individuo(100, 20, 40, 40));
        personagensDeHumanos.add(new Individuo(20, 20, 40, 20));
        personagensDeHumanos.add(new Individuo(20, 80, 60, 40));
        personagensDeHumanos.add(new Individuo(20, 80, 40, 60));
        personagensDeHumanos.add(new Individuo(65, 65, 65, 0));
        personagensDeHumanos.add(new Individuo(40, 60, 30, 70));

        return personagensDeHumanos;

    }


    public void humanosVsMaquina(Individuo melhorIndividuoMaquina, List<Individuo> individuosHumanos){



        Log log = new Log("final", "final");

        int contHumanos = 0;
        int contMaquina = 0;

        int numeroDeLutasEmDiferentesAmbientes = 3;

        for (Individuo individuoDoHumano : individuosHumanos){

            for (int i = 0 ; i < numeroDeLutasEmDiferentesAmbientes; i++){

                Combate combate = new Combate();

                log.getLogger().info("=====================");
                log.getLogger().info("batalha entre: ");
                log.getLogger().info("individuo humano: " + individuoDoHumano);
                log.getLogger().info("individuo maquina: " + melhorIndividuoMaquina);
                Individuo vencedor = combate.batalha(individuoDoHumano, melhorIndividuoMaquina, log);

                if(vencedor != null && vencedor.equals(individuoDoHumano)){
                    contHumanos++;
                }else if(vencedor != null && vencedor.equals(melhorIndividuoMaquina)){
                    contMaquina++;
                }

                log.getLogger().info("=====================");

                individuoDoHumano.setNumVitorias(0);
                melhorIndividuoMaquina.setNumVitorias(0);

            }

            log.getLogger().info("vitorias da maquina:" + contMaquina);
            log.getLogger().info("vitorias dos humanos:" + contHumanos);
        }

    }


    /**
     * depois com o algoritmo convergir
     * tenho q comparar com o personagem ideal
     * como vai ser esse personagem?
     * tem duas ideias em mente.
     * 1 - calcular matematicamente o personagem ideal
     * como? eu ainda não sei
     * 2 - pegar todos os personagens de todas as gerações
     * e submeter ao processo de todas as batalhas
     *      *
     * **/

    public static void main(String[] args) {

        Main m = new Main();
        //m.firstTestConvertion();

        m.testeMelhorFilho();

    }


}
