package enuns;


//força, inteligencia, agilidade, destreza
public enum Classe {
    //se eu realmente precisar de mais uma classe, usar o Defensor -> atributo resistencia
    GUERREIRO, MAGO, ASSASSINO, ARQUEIRO, DEFENSOR;
}
