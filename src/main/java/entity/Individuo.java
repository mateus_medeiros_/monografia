package entity;

import controle.Log;
import enuns.Classe;

import java.util.*;
import java.util.logging.Logger;

public class Individuo {

    private Logger log = Log.logger;

    public static Comparator<Individuo> comparatorNumVitorias = new Comparator<Individuo>() {
        public int compare(Individuo c1, Individuo c2) {
            return c1.getNumVitorias().compareTo(c2.getNumVitorias());
        }
    };

    private static final Integer pontos = 200;

    private static int identificador = 0;

    private Random gerador;

    private Integer id;
    private Classe classe;

    private Integer numVitorias;

    private Integer forca;
    private Integer inteligencia;
    private Integer agilidade;
    private Integer destreza;


    public Individuo() {

        List<Integer> list = distribuaAleatoriamente(pontos);

        identificador = identificador + 1;
        id = identificador;

        numVitorias = 0;

        forca = list.get(0);
        inteligencia = list.get(1);
        agilidade = list.get(2);
        destreza = list.get(3);

        determinaClasse(list);

    }

    public Individuo(Integer id,
                     Classe classe,
                     Integer numVitorias,
                     Integer forca,
                     Integer inteligencia,
                     Integer agilidade,
                     Integer destreza) {
        this.id = id;
        this.classe = classe;
        this.numVitorias = numVitorias;
        this.forca = forca;
        this.inteligencia = inteligencia;
        this.agilidade = agilidade;
        this.destreza = destreza;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        Individuo other = (Individuo) obj;

        if(other.getForca().equals(this.getForca())
            && other.getInteligencia().equals(this.getInteligencia())
                && other.getAgilidade().equals(this.getAgilidade())
                    && other.getDestreza().equals(this.getDestreza())){
            return true;
        }

        return false;
    }

    private void determinaClasse(List<Integer> list){

        int posicao = list.indexOf(Collections.max(list));

        switch (posicao){
            case 0:
                    classe = Classe.GUERREIRO;
                break;

            case 1:
                    classe = Classe.MAGO;
                break;

            case 2:
                    classe = Classe.ASSASSINO;
                break;
            case 3:
                    classe = Classe.ARQUEIRO;
                break;
        }
    }

    //SÓ USAR ESSE MÉTODO QUEM FOR CLONE
    public void sofraComAmbiente(Ambiente ambiente){

        Data dataForca = ambiente.getDatas().get(0);
        Data dataInteligencia = ambiente.getDatas().get(1);
        Data dataAgilidade = ambiente.getDatas().get(2);
        Data dataDestreza = ambiente.getDatas().get(3);


        if(dataForca.getGanhaOuPerde() == 1){
            forca = forca + dataForca.getValor();
        }else if(dataForca.getGanhaOuPerde() == 0){
            forca = forca - dataForca.getValor();

            if(forca < 0){
                forca = 0;
            }
        }

        if(dataInteligencia.getGanhaOuPerde() == 1){
            inteligencia = inteligencia + dataInteligencia.getValor();
        }else if(dataInteligencia.getGanhaOuPerde() == 0){
            inteligencia = inteligencia - dataInteligencia.getValor();

            if(inteligencia < 0){
                inteligencia = 0;
            }

        }

        if(dataAgilidade.getGanhaOuPerde() == 1){
            agilidade = agilidade + dataAgilidade.getValor();
        }else if(dataAgilidade.getGanhaOuPerde() == 0){
            agilidade = agilidade - dataAgilidade.getValor();

            if(agilidade < 0){
                agilidade = 0;
            }
        }

        if(dataDestreza.getGanhaOuPerde() == 1){
            destreza = destreza + dataDestreza.getValor();
        }else if(dataDestreza.getGanhaOuPerde() == 0){
            destreza = destreza - dataDestreza.getValor();

            if(destreza < 0){
                destreza = 0;
            }
        }

    }

    @Override
    public Individuo clone(){

        Individuo individuoClone = new Individuo(this.id,
                                                this.classe,
                                                this.numVitorias,
                                                this.forca,
                                                this.inteligencia,
                                                this.agilidade,
                                                this.destreza);

        return individuoClone;
    }

    private List<Integer> distribuaAleatoriamente(Integer pontos){

        gerador = new Random();

        int poder = pontos;

        List<Integer> list = new ArrayList<Integer>();

        for (int i = 0; i <= 2; i++){

            // 201 -> então o metodo vai buscar aletoriamente de 0 à 200
            int num = gerador.nextInt(poder + 1);

            list.add(num);
            poder = poder - num;
        }

        list.add(poder);

        return list;
    }

    //todo -  ideia interessante, não aplicar mutação para todo mundo;
    //todo -  Não somos obrigados a aplicar os 200 pontos conforme o enunciado
    //todo -  isso faz todo o sentido para o trabalho, uma vez que a intuição humana tende a pensar
    //todo -  que o que possui os 200 pontos preenchido tem mais chances de vitória
    public void mutacao(Logger log){

        log.info("========================================");
        log.info("individuo [" +this.id+  "] sofreu mutação");
        log.info("antes: " + this);

        Integer sobra = pontos - somaAtributos();

        List<Integer> list = distribuaAleatoriamente(sobra);

        forca = forca + list.get(0);
        inteligencia = inteligencia + list.get(1);
        agilidade = agilidade + list.get(2);
        destreza = destreza + list.get(3);

        log.info("depois: " + this);
        log.info("========================================");

    }

    public Integer somaAtributos(){
        return (forca + inteligencia + agilidade + destreza);
    }


    public boolean valido(){
        if(somaAtributos() <= 200){
            return true;
        }
        return false;
    }

    //construtor para a criação de filhos
    public Individuo(Integer forca, Integer inteligencia, Integer agilidade, Integer destreza) {

        identificador = identificador + 1;
        this.id = identificador;

        numVitorias = 0;

        this.forca = forca;
        this.inteligencia = inteligencia;
        this.agilidade = agilidade;
        this.destreza = destreza;

        List<Integer> list = Arrays.asList(forca, inteligencia, agilidade, destreza);

        determinaClasse(list);
    }


    public Integer incrementVitoria() {
        return numVitorias = numVitorias + 1;
    }


    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getForca() {
        return forca;
    }

    public void setForca(Integer forca) {
        this.forca = forca;
    }

    public Integer getInteligencia() {
        return inteligencia;
    }

    public void setInteligencia(Integer inteligencia) {
        this.inteligencia = inteligencia;
    }

    public Integer getAgilidade() {
        return agilidade;
    }

    public void setAgilidade(Integer agilidade) {
        this.agilidade = agilidade;
    }

    public Integer getDestreza() {
        return destreza;
    }

    public void setDestreza(Integer destreza) {
        this.destreza = destreza;
    }

    public Integer getNumVitorias() {
        return numVitorias;
    }

    public void setNumVitorias(Integer numVitorias) {
        this.numVitorias = numVitorias;
    }

    @Override
    public String toString() {
        return "Individuo{" +
                " id=" + id +
                ", numVitorias=" + numVitorias +
                ", classe=" + classe +
                ", forca=" + forca +
                ", inteligencia=" + inteligencia +
                ", agilidade=" + agilidade +
                ", destreza=" + destreza +
                '}';
    }

}
