package entity;

import enuns.Classe;
import enuns.Clima;
import enuns.Terreno;


import java.util.*;


public class Ambiente {

    private Classe classe;
    private Terreno terreno;
    private Clima clima;
    private List<Data> datas;

    public Ambiente() {
    }

    public Ambiente(Classe classe, Terreno terreno, Clima clima, List<Integer> ganhaOuPerdeLista) {

        Random gerador = new Random();

        datas = new ArrayList<Data>();

        this.classe = classe;
        this.terreno = terreno;
        this.clima = clima;

        for (Integer ganhaOuPerde: ganhaOuPerdeLista){

            //variação de 10 em cada atributo
            int num = gerador.nextInt(11);

            Data data = new Data(ganhaOuPerde, num);

            datas.add(data);
        }
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public Terreno getTerreno() {
        return terreno;
    }

    public void setTerreno(Terreno terreno) {
        this.terreno = terreno;
    }

    public Clima getClima() {
        return clima;
    }

    public void setClima(Clima clima) {
        this.clima = clima;
    }

    public List<Data> getDatas() {
        return datas;
    }

    public void setDatas(List<Data> datas) {
        this.datas = datas;
    }

    @Override
    public String toString() {
        return "Ambiente{" +
                "datas=" + datas +
                '}' + '\n';
    }
}

