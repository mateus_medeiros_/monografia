package entity;

public class Data {

    private Integer ganhaOuPerde;
    private Integer valor;

    public Data() {
    }

    public Data(Integer ganhaOuPerde, Integer valor) {
        this.ganhaOuPerde = ganhaOuPerde;
        this.valor = valor;
    }

    public Integer getGanhaOuPerde() {
        return ganhaOuPerde;
    }

    public void setGanhaOuPerde(Integer ganhaOuPerde) {
        this.ganhaOuPerde = ganhaOuPerde;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Data{" +
                "ganhaOuPerde=" + ganhaOuPerde +
                ", valor=" + valor +
                '}';
    }
}
