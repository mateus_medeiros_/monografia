package entity;

import controle.Combate;
import controle.Genetica;
import controle.Log;
import controle.MatrizBase;
import entity.Individuo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


public class Geracao {

    private Integer id;
    private List<Individuo> individuoList;
    private List<Individuo> filhosList;

    private Logger log;

    private static int identificador = 0;

    public Geracao(Integer numerosDeIndividuos, List<Individuo> filhos){

        identificador = identificador + 1;
        this.id = identificador;

        Log log = new Log("geracao"+ id, "geracao" + id);

        this.log = log.getLogger();

        this.individuoList = new ArrayList<Individuo>();
        this.filhosList = new ArrayList<Individuo>();

        if(filhos != null){
            numerosDeIndividuos = numerosDeIndividuos - filhos.size();
            individuoList.addAll(filhos);
        }

        for (int i = 0; i < numerosDeIndividuos; i++){

            Individuo individuo = new Individuo();
            individuoList.add(individuo);
        }
    }


    public List<Individuo> vencedoresDessaGeracao(Integer numDePais){

        Collections.sort(this.individuoList, Individuo.comparatorNumVitorias);
        Collections.reverse(this.individuoList);

        if(numDePais > individuoList.size() ){

            System.out.println("não foi possivel extrair " + numDePais +
                                    " pais individuos dessa geração");
            return null;
        }


        return individuoList.subList(0, numDePais);
    }

    public void logVencedores(List<Individuo> pais){

        log.info("==========================");
        log.info("Vencedores dessa geração: ");

        for (Individuo individuo: pais){
            log.info("vencedor" + individuo);
        }

        log.info("==========================");
    }


    public void logFilhos(List<Individuo> filhos){

        log.info("==========================");
        log.info("Filhos dessa geração: ");

        for (Individuo individuo: filhos){
            log.info("filho" + individuo);
        }

        log.info("==========================");
    }

    public List<Individuo> filhosDessaGeracao(){

        List<Individuo> filhos = new ArrayList<Individuo>();

        Integer porcentagem = porcentagemDeIndividuos(10);
        Integer qtd = verificaPar(porcentagem);

        List<Individuo> pais = vencedoresDessaGeracao(qtd);

        Genetica genetica = new Genetica();

        int tam = pais.size() / 2;

        for (int i = 0; i < tam; i++){

            Individuo pai = pais.get(i);
            Individuo mae = pais.get(i+1);

            filhos.addAll(genetica.cruzamento(pai, mae));

        }

        logVencedores(pais);

        // dos 5 filhos gerados 2 sofrerão mutação
        genetica.mutacaoAleatoria(filhos, 2, log);

        logFilhos(filhos);

        filhosList.addAll(filhos);

        return filhos;
    }


    public Individuo melhorFilho(){

        Integer max = 0;
        Individuo melhorFilho = null;

        for (Individuo individuo1: getFilhosList()){

            Integer valor = Collections.frequency(getFilhosList(), individuo1);

            if(valor > max){
                max = valor;
                melhorFilho = individuo1;
            }

        }

        return melhorFilho;
    }


    private Integer porcentagemDeIndividuos(Integer porCento){
        return (individuoList.size() * porCento) / 100;
    }


    private Integer verificaPar(Integer porcentagem){

        if(porcentagem % 2 == 0){
            return porcentagem;
        }

        return porcentagem - 1;
    }

    public void Lutem(){

        List<String> historicoDeLuta = new ArrayList<String>();

        for(Individuo individuoA: individuoList){

            for(Individuo individuoB: individuoList){

                Combate combate = new Combate();

                String chave1 = individuoA.getId()+ "+" + individuoB.getId();
                String chave2 = individuoB.getId()+ "+" + individuoA.getId();

                //Se não contem a historico de luta entre A+B ou B+A faça a luta acontecer
                if(!historicoDeLuta.contains(chave1) && !historicoDeLuta.contains(chave2)){

                    historicoDeLuta.add(chave1);
                    historicoDeLuta.add(chave2);

                    Individuo vencedor = combate.batalha(individuoA, individuoB, null);

                    if(vencedor == null){
                        //tivemos um empate
                        continue;
                    }
                }
            }
        }

    }

    public List<Individuo> getFilhosList() {
        return filhosList;
    }

    public void setFilhosList(List<Individuo> filhosList) {
        this.filhosList = filhosList;
    }

    public List<Individuo> getIndividuoList() {
        return individuoList;
    }

    public void setIndividuoList(List<Individuo> individuoList) {
        this.individuoList = individuoList;
    }

}
